ARG IMAGE_NAME=registry.fedoraproject.org/fedora
ARG IMAGE_TAG=40

FROM ${IMAGE_NAME}:${IMAGE_TAG} as builder

ARG GIT_REPO_URL=https://github.com/eclipse-ibeji/freyja.git
ARG GIT_REPO_REFS=main
ARG BUILD_TYPE=release

ENV FREYJA_HOME=/etc/freyja

RUN dnf install -y \
git \
gcc \
protobuf-devel \
protobuf-compiler \
rust \
cargo \
openssl-devel \
cmake

RUN git clone -b ${GIT_REPO_REFS} ${GIT_REPO_URL} /tmp/freyja

WORKDIR /tmp/freyja

RUN cargo build --${BUILD_TYPE}

RUN cp -R target/${BUILD_TYPE} /tmp/build && \
cp $(find /tmp/build -name mock_digital_twin_config.default.json) /tmp/build/mock_digital_twin_config.default.json

FROM ${IMAGE_NAME}:${IMAGE_TAG}

ENV FREYJA_HOME=/etc/freyja
 
COPY --from=builder /tmp/build/mock-digital-twin /usr/local/bin/freyja-mock-digital-twin
COPY --from=builder /tmp/build/mock-mapping-service /usr/local/bin/freyja-mock-mapping-service
COPY --from=builder /tmp/build/mock_digital_twin_config.default.json ${FREYJA_HOME}/config/mock_digital_twin_config.default.json

ENTRYPOINT ["/bin/bash", "-c"]
