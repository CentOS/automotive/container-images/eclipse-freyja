# Eclipse Freyja

https://github.com/eclipse-ibeji/freyja

## Building

Build the image by running:

```sh
podman build -t localhost/eclipse-freyja:latest .
```

## Running

Pre-built image: `quay.io/centos-sig-automotive/eclipse-freyja:latest`.

TBD

## Using

TBD

## License

[MIT](./LICENSE)
